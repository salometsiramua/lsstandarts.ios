# LSSTandarts.iOS #

#სარჩევი

- [String Extenstion](LSStrandarts/Extensions-String.swift) - სტრინგის საბსტრინგი, იური წევრი fullword[i], 
- [UIColor Extenstion](LSStrandarts/Extensions-UIColor.swift) - UIColor init from hexString
- [Device Class](LSStrandarts/Extensions-UIDevice.swift) - Device class რომელიც გვაძლევს სრულ ინფორმაციას დივაისზე რომელზეც გაშვებულია აპლიკაცია (ვიყენებთ resizable font-ში)
- [Resizable Font Extenstion](LSStrandarts/Extensions-ResizableFont.swift) - device-ის ეკრანის ზომის მიხედვით უცვლის ზომას სხვადასხვა ობიექტებს (UITextview, UILable, UIButton..)
- [UIScrollView Extenstion](LSStrandarts/Extensions-UIScrollView.swift) - scroll view paging-ისთვის extention, რომელიც გვიბრუნებს გვერდს სად უნდა გაჩერდეს სქროლი
- [UIView Extension](LSStrandarts/Extensions-UIView.swift) - keyboard-ის ჩახურვა გარეთ კლიკზე, default loadingis დასმა ვიუს ცენტრში