//
//  Extensions-UIScrollView.swift
//  LSStrandarts
//
//  Created by Girls on 10/10/16.
//  Copyright © 2016 Girls. All rights reserved.
//

import UIKit

extension UIScrollView {
    var currentPage: Int {
        return Int((self.contentOffset.x + (0.5*self.frame.size.width))/self.frame.width)
    }
}

