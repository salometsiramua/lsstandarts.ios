//
//  Extensions-UIView.swift
//  LSStrandarts
//
//  Created by Girls on 10/10/16.
//  Copyright © 2016 Girls. All rights reserved.
//

import UIKit


extension UIView{
    
    func addTapOutsideCloseKeyboard(){
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIView.dismissKeyboard))
        self.addGestureRecognizer(tap)
        
    }
    
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        self.endEditing(true)
    }
    func addLoader()  ->  UIActivityIndicatorView{
        let loadingIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
        loadingIndicator.frame.size = CGSize(width: 50, height: 50)
        if Device.current.family == Device.family.iPad {
            var c = self.center
            c.x = ((UIScreen.main.bounds.width * 493 / 768) )/2
            loadingIndicator.center = c
        }else{
            loadingIndicator.center = self.center
        }
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        loadingIndicator.startAnimating();
        
        self.addSubview(loadingIndicator)
        return loadingIndicator
    }
    
    
}
