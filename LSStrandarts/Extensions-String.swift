//
//  Extensions-String.swift
//  LSStrandarts
//
//  Created by Girls on 10/10/16.
//  Copyright © 2016 Girls. All rights reserved.
//

import UIKit


extension String {
    
    var length : Int {
        return self.characters.count
    }
    
    func digitsOnly() -> String{
        let stringArray = self.components(
            separatedBy: CharacterSet.decimalDigits.inverted)
        let newString = stringArray.joined(separator: "")
        
        return newString
    }
    
    var isEmailValid: Bool? {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,20}"
        let emailTest  = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    var isPasswordValid: Bool? {
        return (self.length >= 6 && self.length <= 64)
    }
    
    var isMobileValid: Bool? {
        return self.length >= 9
    }
    var isPersonalIDValid:Bool? {
        return self.length == 11
    }
    
    func index(from: Int) -> Index {
        return self.index(startIndex, offsetBy: from)
    }
    
    func substring(from: Int) -> String {
        let fromIndex = index(from: from)
        return substring(from: fromIndex)
    }
    
    func substring(to: Int) -> String {
        let toIndex = index(from: to)
        return substring(to: toIndex)
    }
    
    func substring(with r: Range<Int>) -> String {
        let startIndex = index(from: r.lowerBound)
        let endIndex = index(from: r.upperBound)
        return substring(with: startIndex..<endIndex)
    }
    subscript (i: Int) -> Character {
        var index = i
        if(i < 0){
            index = 0
        }else if(i >= self.characters.count){
            index = self.characters.count - 1
        }
        let charAtIndex = self[self.index(self.startIndex, offsetBy: index)]
        
        return (charAtIndex)
    }

    
}

