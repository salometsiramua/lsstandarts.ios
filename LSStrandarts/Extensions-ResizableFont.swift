import UIKit

extension UILabel {
    @IBInspectable
    var adjustFontToRealIPhoneSize: Bool {
        set {
            if newValue {
                if let currentFont = font {
                    let realFontSize = Device.FontSizeScale.scale(fontSize: currentFont.pointSize)
                    font = currentFont.withSize(realFontSize)
                }
            }
        }
        get {
            return false
        }
    }
}

extension UIButton {
    @IBInspectable
    var adjustFontToRealIPhoneSize: Bool {
        set {
            if newValue {
                if let label = titleLabel, let currentFont = titleLabel?.font {
                    let realFontSize = Device.FontSizeScale.scale(fontSize: currentFont.pointSize)
                    label.font = currentFont.withSize(realFontSize)
                }
            }
        }
        get {
            return false
        }
    }
}

extension UITextField {
    @IBInspectable
    var adjustFontToRealIPhoneSize: Bool {
        set {
            if let currentFont = font {
                let realFontSize = Device.FontSizeScale.scale(fontSize: currentFont.pointSize)
                font = currentFont.withSize(realFontSize)
            }
        }
        get {
            return false
        }
    }
}

extension UITextView {
    @IBInspectable
    var adjustFontToRealIPhoneSize: Bool {
        set {
            if newValue {
                if let currentFont = font {
                    let realFontSize = Device.FontSizeScale.scale(fontSize: currentFont.pointSize)
                    font = currentFont.withSize(realFontSize)
                }
            }
        }
        
        get {
            return false
        }
    }
}



//import UIKit
//
//enum ProjectFont: String {
//    case base = "MyGeocell-Regular"
//    
//    case notosansbold = "NotoSans-Bold"
//    case notosansregular = "NotoSans"
//    case bpgPhoneSans = "BPGPhoneSans"
//    case bpgPhoneSansBold = "BPGPhoneSans-Bold"
//    case bpgArial2010 = "BPGArial2010"
//    case bpgGEL = "!BPGGEL"
//    
//    func with(size: CGFloat) -> UIFont {
//        return UIFont(name: self.rawValue, size: size)!
//    }
//    
//    static func printFontNames() {
//        for family in UIFont.familyNames {
//            print("\(family)")
//            
//            for name in UIFont.fontNames(forFamilyName: family) {
//                print("   \(name)")
//            }
//        }
//    }
//}

// გამოყენების მაგალითი

// let font = UIFont(name: ProjectFont.base.rawValue, size: 15)
// let font = ProjectFont.base.with(size: 15)

